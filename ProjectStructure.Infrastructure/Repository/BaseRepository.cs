﻿using AutoMapper;
using EF.Core.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Infrastructure.Repository
{
    public abstract class BaseRepository
    {
        private protected readonly EFdbContext _context;
        private protected readonly IMapper _mapper;

        public BaseRepository(EFdbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
