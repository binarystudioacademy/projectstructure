﻿using AutoMapper;
using EF.Core.DAL.Context;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Application.Common.Exceptions;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Infrastructure.Repository
{
    public class TeamRepository : BaseRepository, IRepository<TeamViewModelDTO>
    {
        public TeamRepository(EFdbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<IList<TeamViewModelDTO>> GetAll()
        {
            var teams = await _context.Teams.ToListAsync();
            var result = _mapper.Map<List<TeamViewModelDTO>>(teams);
            return result;
        }

        public async Task<TeamViewModelDTO> GetById(int id)
        {
            var team = await _context.Teams.AsNoTracking()
                .FirstOrDefaultAsync(t => t.Id == id);

            return _mapper.Map<TeamViewModelDTO>(team);
        }

        public async System.Threading.Tasks.Task Create(TeamViewModelDTO item)
        {
            var newTeam = new Team
            {
                Name = item.Name,
                CreatedAt = DateTime.Now
            };

            _context.Teams.Add(newTeam);

            await _context.SaveChangesAsync();          
        }


        public async System.Threading.Tasks.Task Delete(int id)
        {
            
            var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Update(TeamViewModelDTO teamDto)
        {
            var postTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == teamDto.Id);

            if (postTeam == null)
            {
                throw new NotFoundException(nameof(Team), teamDto.Id);
            }

            postTeam.Name = teamDto.Name;

            _context.Teams.Update(postTeam);
            await _context.SaveChangesAsync();
        }      
    }
}