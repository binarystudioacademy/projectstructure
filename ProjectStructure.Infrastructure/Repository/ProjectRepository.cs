﻿using ProjectStructure.Application.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Application.Interfaces;

namespace ProjectStructure.Infrastructure.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        readonly List<Project> projectList = new List<Project>
        {
            new (){Id = 0, AuthorId = 1, TeamId = 0, Name = "next generation", Description = "Qui ex architecto qui.", Deadline = new DateTime(2021,12,25), CreatedAt = new DateTime(2021,11,25) },
            new (){Id = 2, AuthorId = 2, TeamId = 0, Name = "Data", Description = "Qui ex architecto qui.", Deadline = new DateTime(2021,12,25), CreatedAt = new DateTime(2021,11,25) },
            new (){Id = 3, AuthorId = 2, TeamId = 0, Name = "WoW", Description = "old", Deadline = new DateTime(2019,10,12), CreatedAt = new DateTime(2015,10,25) },
            new (){Id = 4, AuthorId = 3, TeamId = 0, Name = "Aion", Description = "classic", Deadline = new DateTime(2021,09,25), CreatedAt = new DateTime(2015,11,25) },
            new (){Id = 5, AuthorId = 3, TeamId = 0, Name = "L2", Description = "mobile", Deadline = null, CreatedAt = new DateTime(2018,11,25) }
        };

        public async Task<IList<Project>> GetAll()
        {
            return projectList;
        }       
               
        public async Task<Project> GetById(int id)
        {
            var result = projectList.FirstOrDefault(x => x.Id == id);
            return result;
        }

        public async Task<int> Create(Project project)
        {
            project.Id = new Random().Next(10,100);
            projectList.Add(project);
            return project.Id;
        }

        public async Task Delete(int id)
        {
            var project = projectList.FirstOrDefault(x => x.Id == id);
            if (project == null)
            {
                throw new Exception("Project not found");
            }

            projectList.Remove(project);
        }      

        public async Task<Project> Update(Project item)
        {
            throw new NotImplementedException();
        }

        Task IRepository<Project>.Create(Project item)
        {
            throw new NotImplementedException();
        }

        Task IRepository<Project>.Update(Project item)
        {
            throw new NotImplementedException();
        }
    }
}