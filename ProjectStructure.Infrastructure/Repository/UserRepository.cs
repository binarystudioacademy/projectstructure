﻿using AutoMapper;
using EF.Core.DAL.Context;
using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Application.Common.Exceptions;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Infrastructure.Repository
{
    public class UserRepository : BaseRepository, IRepository<UserViewModel>
    {
        public UserRepository(EFdbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<IList<UserViewModel>> GetAll()
        {
            var teams = await _context.Users.ToListAsync();
            var result = _mapper.Map<List<UserViewModel>>(teams);
            return result;
        }

        public async Task<UserViewModel> GetById(int id)
        {
            var team = await _context.Teams.AsNoTracking()
                .FirstOrDefaultAsync(t => t.Id == id);

            return _mapper.Map<UserViewModel>(team);
        }

        public async System.Threading.Tasks.Task Create(UserViewModel item)
        {
            var user = _mapper.Map<User>(item);

            await _context.Users.AddAsync(user);
                       
            await _context.SaveChangesAsync();
        }


        public async System.Threading.Tasks.Task Delete(int id)
        {

            var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Update(TeamViewModelDTO teamDto)
        {
            var postTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == teamDto.Id);

            if (postTeam == null)
            {
                throw new NotFoundException(nameof(Team), teamDto.Id);
            }

            postTeam.Name = teamDto.Name;

            _context.Teams.Update(postTeam);
            await _context.SaveChangesAsync();
        }

              

        public async System.Threading.Tasks.Task Update(UserViewModel item)
        {
            throw new NotImplementedException();
        }
    }
}
