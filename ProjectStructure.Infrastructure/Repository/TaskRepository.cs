﻿using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models;
using ProjectStructure.Application.Models.ProjectTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Infrastructure.Repository
{
    public class TaskRepository : IRepository<ProjectTask>
    {
        readonly List<ProjectTask> tasksList = new List<ProjectTask>
        {
            new (){Id = 0,ProjectId = 0, PerformerId =37, Name ="Generic Steel Towels", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 1,ProjectId = 0, PerformerId =99, Name ="Producer", Description = "Sit ut quo tempora.", State=TaskState.Сreated, CreatedAt = new DateTime(2018,10,01), FinishedAt = new DateTime(2019,10,01)},
            new (){Id = 2,ProjectId = 0, PerformerId =19, Name ="Steel Towels", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 3,ProjectId = 0, PerformerId =37, Name ="Generic s", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 4,ProjectId = 0, PerformerId =37, Name ="Towels", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 5,ProjectId = 0, PerformerId =37, Name ="G Towels", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 6,ProjectId = 0, PerformerId =37, Name ="Steel Towels -2", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 7,ProjectId = 0, PerformerId =37, Name ="Generic Steel", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 8,ProjectId = 0, PerformerId =37, Name ="Towels- 5", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
            new (){Id = 9,ProjectId = 0, PerformerId =37, Name ="Generic Steel Towels", Description = "Magni fugit ut consequatur sit voluptatem eum.", State=TaskState.Unfinished, CreatedAt = new DateTime(2018,09,01), FinishedAt = null},
        };

        public async Task<IList<ProjectTask>> GetAll()
        {
            return tasksList;
        }

        public async Task<ProjectTask> GetById(int id)
        {
            var result = tasksList.FirstOrDefault(x => x.Id == id);
            return result;
        }

        public async Task<int> Create(ProjectTask item)
        {
            item.Id = new Random().Next(10, 100);
            tasksList.Add(item);
            return item.Id;
        }

        public async Task Delete(int id)
        {
            var item = tasksList.FirstOrDefault(x => x.Id == id);
            if (item == null)
            {
                throw new Exception("Item not found");
            }

            tasksList.Remove(item);
        }     
                
        public async Task<ProjectTask> Update(ProjectTask newTask)
        {
            throw new Exception(); 
        }

        Task IRepository<ProjectTask>.Create(ProjectTask item)
        {
            throw new NotImplementedException();
        }

        Task IRepository<ProjectTask>.Update(ProjectTask item)
        {
            throw new NotImplementedException();
        }
    }
}
