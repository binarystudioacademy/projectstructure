﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Application.Features.Team.Commands.Create;
using ProjectStructure.Application.Features.Team.Commands.Delete;
using ProjectStructure.Application.Features.Team.Commands.Update;
using ProjectStructure.Application.Features.Team.Queries.GetAll;
using ProjectStructure.Application.Features.Team.Queries.GetById;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await Mediator.Send(new GetAllTeamsQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await Mediator.Send(new GetTeamByIdQuery { Id = id });
            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(CreateTeamCommand command)
        {
            var result = await Mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await Mediator.Send(new DeleteTeamByIdCommand { Id = id });
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(new { ErrorMessage = e.Message });
            }

            return Ok();
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> Update (int id, UpdateTeamCommand command)
        {
            command.Id = id;

            await Mediator.Send(command);

            return Ok();
        }
    }
}
