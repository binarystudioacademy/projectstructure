﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Application.Features.ProjectTask.Commands.CreateTask;
using ProjectStructure.Application.Features.ProjectTask.Commands.Delete;
using ProjectStructure.Application.Features.ProjectTask.Queries.GetAll;
using ProjectStructure.Application.Features.ProjectTask.Queries.GetById;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class TasksController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await Mediator.Send(new GetTasksQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await Mediator.Send(new GetTaskByIdQuery { Id = id });
            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(CreateTaskCommand command)
        {
            var result = await Mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await Mediator.Send(new DeleteTaskByIdCommand { Id = id });
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(new { ErrorMessage = e.Message });
            }

            return Ok();
        }

        //[HttpPut("{id}")]
        //public async Task<IActionResult> Update([FromBody] UpdateTaskCommand, int id)
        //{

        //}

    }
}
