﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Application.Features.Project.Commands.CreateProject;
using ProjectStructure.Application.Features.Project.Commands.DeleteProject;
using ProjectStructure.Application.Features.Project.Queries.GetProjects;
using System;
using System.Threading.Tasks;

namespace ProjectStructure.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await Mediator.Send(new GetProjectsQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await Mediator.Send(new GetProjectByIdQuery { Id = id });
            return Ok(result);
        }


        [HttpPost("create")]
        public async Task<IActionResult> Create(CreateProjectCommand command)
        {
            var result = await Mediator.Send(command);
            return Ok(result);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete (int id)
        {
            try
            {
                await Mediator.Send(new DeleteProjectCommand { Id = id });
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(new { ErrorMessage = e.Message });
            }
            
            return Ok();
        }
    }
}
