using EF.Core.DAL.Context;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProjectStructure.API.Extensions;
using ProjectStructure.Application.Features.Project.Commands.CreateProject;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Project;
using ProjectStructure.Application.Models.ProjectTask;
using ProjectStructure.Application.Models.Team;
using ProjectStructure.Application.Models.User;
using ProjectStructure.Infrastructure.Repository;


namespace ProjectStructure.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ProjectStructure.API", Version = "v1" });
            });
            services.AddDbContext<EFdbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("EFcoreDB")));
            services.AddMediatR(typeof(CreateProjectCommand).Assembly);
            services.RegisterAutoMapper();
            services.AddScoped<IRepository<Project>, ProjectRepository>();
            services.AddScoped<IRepository<ProjectTask>, TaskRepository>();
            services.AddScoped<IRepository<TeamViewModelDTO>, TeamRepository>();
            services.AddScoped<IRepository<UserViewModel>, UserRepository>();
          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProjectStructure.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
