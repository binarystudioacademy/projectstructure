﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.Application.MappingProfiles;
using System.Reflection;

namespace ProjectStructure.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
