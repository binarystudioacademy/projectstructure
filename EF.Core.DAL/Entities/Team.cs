﻿using System;
using System.Collections.Generic;

namespace EF.Core.DAL.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<Project> Projects { get; set; }
        public List<User> Users { get; set; }      
    }
}