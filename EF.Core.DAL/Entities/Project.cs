﻿using EF.Core.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace EF.Core.DAL.Entities
{
    public class Project 
    {       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public int TeamId { get; set; } 
        public Team Team { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public List<Task> Tasks { get; set; }
                
    }
}
