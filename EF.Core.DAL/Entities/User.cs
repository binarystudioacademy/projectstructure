﻿using System;
using System.Collections.Generic;

namespace EF.Core.DAL.Entities
{
    public  class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
         
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        
    }
}