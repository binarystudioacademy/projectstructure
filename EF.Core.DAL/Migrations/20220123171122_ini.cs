﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EF.Core.DAL.Migrations
{
    public partial class ini : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    TaskStateId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.TaskStateId);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    TaskStateId = table.Column<int>(type: "int", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskStates_TaskStateId",
                        column: x => x.TaskStateId,
                        principalTable: "TaskStates",
                        principalColumn: "TaskStateId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 1, 23, 19, 11, 21, 861, DateTimeKind.Local).AddTicks(433), "Rowe, Braun and Gulgowski" },
                    { 9, new DateTime(2022, 1, 23, 19, 11, 21, 865, DateTimeKind.Local).AddTicks(7004), "Breitenberg - Reilly" },
                    { 8, new DateTime(2022, 1, 23, 19, 11, 21, 865, DateTimeKind.Local).AddTicks(6661), "Flatley - Barrows" },
                    { 7, new DateTime(2022, 1, 23, 19, 11, 21, 865, DateTimeKind.Local).AddTicks(6118), "Prosacco LLC" },
                    { 6, new DateTime(2022, 1, 23, 19, 11, 21, 864, DateTimeKind.Local).AddTicks(8677), "Jacobi - Kris" },
                    { 10, new DateTime(2022, 1, 23, 19, 11, 21, 865, DateTimeKind.Local).AddTicks(7380), "Sporer and Sons" },
                    { 4, new DateTime(2022, 1, 23, 19, 11, 21, 864, DateTimeKind.Local).AddTicks(7966), "Baumbach - Beahan" },
                    { 3, new DateTime(2022, 1, 23, 19, 11, 21, 864, DateTimeKind.Local).AddTicks(7691), "Emard - Douglas" },
                    { 2, new DateTime(2022, 1, 23, 19, 11, 21, 864, DateTimeKind.Local).AddTicks(7188), "Rogahn - Schulist" },
                    { 5, new DateTime(2022, 1, 23, 19, 11, 21, 864, DateTimeKind.Local).AddTicks(8415), "Ryan, Senger and Ferry" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 9, new DateTime(1960, 3, 17, 20, 4, 31, 782, DateTimeKind.Local).AddTicks(8731), "Tomas.Kuhic9@gmail.com", "Nicola.Reinger20", "Kuhic", new DateTime(2022, 1, 23, 19, 11, 21, 911, DateTimeKind.Local).AddTicks(4678), null },
                    { 1, new DateTime(1994, 5, 1, 9, 27, 50, 540, DateTimeKind.Local).AddTicks(9015), "Guy14@gmail.com", "Amie_Ledner", "Walker", new DateTime(2022, 1, 23, 19, 11, 21, 908, DateTimeKind.Local).AddTicks(3430), null },
                    { 2, new DateTime(1988, 10, 22, 12, 36, 55, 406, DateTimeKind.Local).AddTicks(1421), "Darrel69@yahoo.com", "Velva.Senger27", "Rempel", new DateTime(2022, 1, 23, 19, 11, 21, 909, DateTimeKind.Local).AddTicks(3892), null },
                    { 3, new DateTime(1996, 10, 28, 11, 32, 1, 780, DateTimeKind.Local).AddTicks(7124), "Daryl4@hotmail.com", "Justina.Rippin93", "Pouros", new DateTime(2022, 1, 23, 19, 11, 21, 909, DateTimeKind.Local).AddTicks(5784), null },
                    { 4, new DateTime(1998, 1, 11, 22, 5, 4, 890, DateTimeKind.Local).AddTicks(1465), "Bobbie13@hotmail.com", "Marty53", "Gorczany", new DateTime(2022, 1, 23, 19, 11, 21, 909, DateTimeKind.Local).AddTicks(7502), null },
                    { 5, new DateTime(1980, 5, 29, 16, 17, 46, 951, DateTimeKind.Local).AddTicks(9952), "Devin55@yahoo.com", "Maryjane_Ebert", "Streich", new DateTime(2022, 1, 23, 19, 11, 21, 909, DateTimeKind.Local).AddTicks(9087), null },
                    { 6, new DateTime(1959, 12, 16, 23, 28, 38, 351, DateTimeKind.Local).AddTicks(5947), "Angelo_Crooks@hotmail.com", "Maci_Wiegand18", "Crooks", new DateTime(2022, 1, 23, 19, 11, 21, 910, DateTimeKind.Local).AddTicks(676), null },
                    { 7, new DateTime(1990, 11, 20, 19, 53, 11, 440, DateTimeKind.Local).AddTicks(613), "Gail_OHara@gmail.com", "Kade88", "O'Hara", new DateTime(2022, 1, 23, 19, 11, 21, 911, DateTimeKind.Local).AddTicks(683), null },
                    { 8, new DateTime(1970, 10, 1, 9, 37, 25, 573, DateTimeKind.Local).AddTicks(8186), "Roosevelt.Rempel50@gmail.com", "Columbus_Terry20", "Rempel", new DateTime(2022, 1, 23, 19, 11, 21, 911, DateTimeKind.Local).AddTicks(2477), null },
                    { 10, new DateTime(1972, 10, 18, 20, 0, 7, 378, DateTimeKind.Local).AddTicks(6559), "Sue50@yahoo.com", "Orpha_Streich", "Weber", new DateTime(2022, 1, 23, 19, 11, 21, 911, DateTimeKind.Local).AddTicks(7187), null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_UserId",
                table: "Projects",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_TaskStateId",
                table: "Tasks",
                column: "TaskStateId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
