﻿using EF.Core.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace EF.Core.DAL.Context
{
    public class EFdbContext: DbContext
    {
        public EFdbContext(DbContextOptions<EFdbContext> options)
            : base(options)
        { }

        public DbSet<Project> Projects { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<User> Users { get; private set; }
        public DbSet<TaskState> TaskStates { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            modelBuilder.Seed();
        }
    }
}
