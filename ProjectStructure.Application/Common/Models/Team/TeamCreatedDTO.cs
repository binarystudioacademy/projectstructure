﻿namespace ProjectStructure.Application.Common.Team
{
    public sealed class TeamCreatedDTO
    {
        public string Name { get; set; }
    }
}
