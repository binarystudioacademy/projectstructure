﻿using System;

namespace ProjectStructure.Application.Models.Team
{
    public class TeamViewModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
