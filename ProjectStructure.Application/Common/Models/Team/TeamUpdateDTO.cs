﻿namespace EF.Core.Common.DTO.Team
{
    public sealed class TeamUpdateDTO
    { 
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
