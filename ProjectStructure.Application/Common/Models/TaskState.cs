﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Models
{
    public enum TaskState
    {
        Сreated,
        Unfinished,
        Finished,
        Canceled,
    }
}
