﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Team.Queries.GetAll
{
    public class GetAllTeamsQuery : IRequest<IList<TeamViewModelDTO>>
    {

    }

    public class GetAllTeamsCommandHandler : IRequestHandler<GetAllTeamsQuery, IList<TeamViewModelDTO>>
    {
        private readonly IRepository<TeamViewModelDTO> _teamRepository;

        public GetAllTeamsCommandHandler(IRepository<TeamViewModelDTO> teamRepository) => _teamRepository = teamRepository;

        public async Task<IList<TeamViewModelDTO>> Handle(GetAllTeamsQuery request, CancellationToken cancellationToken)
        {
            var result = await _teamRepository.GetAll();
            return result;
        }
    }
}
