﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Team.Queries.GetById
{
    public class GetTeamByIdQuery : IRequest<TeamViewModelDTO>
    {
        public int Id { get; set; }
    }

    public class GetTeamByIdCommandHandler : IRequestHandler<GetTeamByIdQuery, TeamViewModelDTO>
    {
        private readonly IRepository<TeamViewModelDTO> _teamRepository;

        public GetTeamByIdCommandHandler(IRepository<TeamViewModelDTO> teamRepository) => _teamRepository = teamRepository;

        public async Task<TeamViewModelDTO> Handle(GetTeamByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _teamRepository.GetById(request.Id);

            return result;
        }
    }
}
