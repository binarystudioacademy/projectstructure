﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Team.Commands.Update
{
    public class UpdateTeamCommand : IRequest 
    {
        public int Id { get; set; }
        public string Name { get; set; }       
    }

    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand>
    {
        private readonly IRepository<TeamViewModelDTO> _teamRepository;

        public UpdateTeamCommandHandler(IRepository<TeamViewModelDTO> teamRepository)
        {
            _teamRepository = teamRepository;
        }
        public async Task<Unit> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            var updateTeam = new TeamViewModelDTO
            {
                Id = request.Id,
                Name = request.Name
            };

            await _teamRepository.Update(updateTeam);

            return Unit.Value;
        }
    }
}
