﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Team.Commands.Delete
{
    public class DeleteTeamByIdCommand : IRequest
    {
        public int Id { get; set; }
    }
    public class DeleteTeamByIdCommandHandler : IRequestHandler<DeleteTeamByIdCommand>
    {
        private readonly IRepository<TeamViewModelDTO> _teamRepository;

        public DeleteTeamByIdCommandHandler(IRepository<TeamViewModelDTO> teamRepository) => _teamRepository = teamRepository;

        public async Task<Unit> Handle(DeleteTeamByIdCommand request, CancellationToken cancellationToken)
        {
            await _teamRepository.Delete(request.Id);
            return Unit.Value;
        }
    }
}