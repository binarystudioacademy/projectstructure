﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.Team;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Team.Commands.Create
{
    public class CreateTeamCommand : IRequest
    {
        public string Name { get; set; }
    }

    public class CreateTeamCommandHandler : IRequestHandler<CreateTeamCommand>
    {
        private readonly IRepository<TeamViewModelDTO> _teamRepository;

        public CreateTeamCommandHandler(IRepository<TeamViewModelDTO> teamRepository) => _teamRepository = teamRepository;

        public async Task<Unit> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var newTeam = new TeamViewModelDTO
            {
                Name = request.Name,
                CreatedAt = DateTime.Now
            };

            await _teamRepository.Create(newTeam);
            return Unit.Value;
        }
    }
}
