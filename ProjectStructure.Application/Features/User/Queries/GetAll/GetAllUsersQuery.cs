﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models.User;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.User.Queries.GetAll
{
    public class GetAllUsersQuery : IRequest<IList<UserViewModel>>
    {
    }

    public class GetAllUsersRequestHandler : IRequestHandler<GetAllUsersQuery, IList<UserViewModel>>
    {
        private readonly IRepository<UserViewModel> _userRepository;

        public GetAllUsersRequestHandler(IRepository<UserViewModel> userRepository) => _userRepository = userRepository;

        public async Task<IList<UserViewModel>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            var result = await _userRepository.GetAll();
            return result;
        }
    }
}
