﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.ProjectTask.Commands.Delete
{
    public class DeleteTaskByIdCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeleteTaskByIdCommandHandler : IRequestHandler<DeleteTaskByIdCommand>
    {
        private readonly IRepository<Models.ProjectTask.ProjectTask> _taskRepository;

        public DeleteTaskByIdCommandHandler(IRepository<Models.ProjectTask.ProjectTask> taskRepository) => _taskRepository = taskRepository;

        public async Task<Unit> Handle(DeleteTaskByIdCommand request, CancellationToken cancellationToken)
        {
            await _taskRepository.Delete(request.Id);

            return Unit.Value;
        }
    }
}