﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.ProjectTask.Commands.Update
{
    public class UpdateTaskCommand : IRequest
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }

    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand>
    {
        private readonly IRepository<Models.ProjectTask.ProjectTask> _taskRepository;

        public UpdateTaskCommandHandler(IRepository<Models.ProjectTask.ProjectTask> taskRepository) => _taskRepository = taskRepository;

        public async Task<Unit> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            var newTask = new Models.ProjectTask.ProjectTask
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                FinishedAt = request.FinishedAt,
                ProjectId = request.ProjectId,
                PerformerId = request.PerformerId,
                State = request.State
            };

            await _taskRepository.Update(newTask);
            return Unit.Value;
        }
    }
}
