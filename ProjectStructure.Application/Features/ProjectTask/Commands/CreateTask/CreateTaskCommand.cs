﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using ProjectStructure.Application.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.ProjectTask.Commands.CreateTask
{
    public class CreateTaskCommand : IRequest
    {       
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
    }

    public class CreateTaskCommandHendler : IRequestHandler<CreateTaskCommand>
    {
        private readonly IRepository<Models.ProjectTask.ProjectTask> _taskRepository;

        public CreateTaskCommandHendler(IRepository<Models.ProjectTask.ProjectTask> taskRepository) => _taskRepository = taskRepository;

        public async Task<Unit> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {
            var task = new Models.ProjectTask.ProjectTask()
            {
                ProjectId = request.ProjectId,
                PerformerId = request.PerformerId,
                Name = request.Name,
                Description = request.Description,
                State = request.State,
                CreatedAt = DateTime.Now
            };

            await _taskRepository.Create(task);
            return Unit.Value;
        }
    }
}
