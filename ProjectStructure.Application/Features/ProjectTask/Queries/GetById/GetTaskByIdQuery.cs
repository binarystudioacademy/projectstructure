﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProjectStructure.Application.Interfaces;

namespace ProjectStructure.Application.Features.ProjectTask.Queries.GetById
{
    public class GetTaskByIdQuery : IRequest<Models.ProjectTask.ProjectTask>
    {
        public int Id { get; set; }
    }

    public class GetByIdQueryHandrel : IRequestHandler<GetTaskByIdQuery, Models.ProjectTask.ProjectTask>
    {
        private readonly IRepository<Models.ProjectTask.ProjectTask> _taskRepository;

        public GetByIdQueryHandrel(IRepository<Models.ProjectTask.ProjectTask> taskRepository) => _taskRepository = taskRepository;
        
        public async Task<Models.ProjectTask.ProjectTask> Handle(GetTaskByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _taskRepository.GetById(request.Id);
            return result;
        }
    }
}
