﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.ProjectTask.Queries.GetAll
{
    public class GetTasksQuery : IRequest<IList<Models.ProjectTask.ProjectTask>>
    {
    }

    public class GetTasksRequestHandler : IRequestHandler<GetTasksQuery,IList<Models.ProjectTask.ProjectTask>>
    {
        private readonly IRepository<Models.ProjectTask.ProjectTask> _taskRepository;

        public GetTasksRequestHandler(IRepository<Models.ProjectTask.ProjectTask> taskRepository) => _taskRepository = taskRepository;

        public async Task<IList<Models.ProjectTask.ProjectTask>> Handle(GetTasksQuery request, CancellationToken cancellationToken)
        {
            var result = await _taskRepository.GetAll();
            return result;
        }
    }
}
