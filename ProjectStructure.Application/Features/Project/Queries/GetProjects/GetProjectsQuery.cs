﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Project.Queries.GetProjects
{
    public class GetProjectsQuery : IRequest<IList<Models.Project.Project>>
    {

    }

    public class GetProjectsQueryHandrel : IRequestHandler<GetProjectsQuery, IList<Models.Project.Project>>
    {
        private readonly IRepository<Models.Project.Project> _projectRepository;

        public GetProjectsQueryHandrel(IRepository<Models.Project.Project> projectRepository)
        {
            _projectRepository = projectRepository;
        }
        async Task<IList<Models.Project.Project>> IRequestHandler<GetProjectsQuery, IList<Models.Project.Project>>.Handle(GetProjectsQuery request, CancellationToken cancellationToken)
        {
            var result = await _projectRepository.GetAll();
            return result;
        }

    }
}
