﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Project.Queries.GetProjects
{
    public class GetProjectByIdQuery : IRequest<Models.Project.Project>
    {
        public int Id { get; set; }
    }

    public class GetProjectByIdQueryHandler : IRequestHandler<GetProjectByIdQuery, Models.Project.Project>
    {
        private readonly IRepository<Models.Project.Project> _projectRepository;

        public GetProjectByIdQueryHandler(IRepository<Models.Project.Project> projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public async Task<Models.Project.Project> Handle(GetProjectByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _projectRepository.GetById(request.Id);
            return result;
        }
    }
}