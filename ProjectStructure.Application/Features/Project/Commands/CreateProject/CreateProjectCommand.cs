﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Project.Commands.CreateProject
{
    public class CreateProjectCommand : IRequest
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }

    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand>
    {
        private readonly IRepository<Models.Project.Project> _projectRepository;

        public CreateProjectCommandHandler(IRepository<Models.Project.Project> tasksRepository)
        {
            _projectRepository = tasksRepository;
        }

        public async Task<Unit> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = new Models.Project.Project()
            {
                AuthorId = request.AuthorId,
                TeamId = request.TeamId,
                Name = request.Name,
                Description = request.Description,
                Deadline = request.Deadline,
                CreatedAt = DateTime.Now
            };

            await _projectRepository.Create(project);
            return Unit.Value;
        }        
    }
}
