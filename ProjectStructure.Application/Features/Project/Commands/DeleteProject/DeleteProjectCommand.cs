﻿using MediatR;
using ProjectStructure.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Features.Project.Commands.DeleteProject
{
    public class DeleteProjectCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeleteProjectCommandHabdler : IRequestHandler<DeleteProjectCommand>
    {
        private readonly IRepository<Models.Project.Project> _projectRepository;

        public DeleteProjectCommandHabdler(IRepository<Models.Project.Project> projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<Unit> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            await _projectRepository.Delete(request.Id);

            return Unit.Value;
        }
    }
}
