﻿using AutoMapper;
using EF.Core.Common.DTO.Team;
using EF.Core.DAL.Entities;
using ProjectStructure.Application.Common.Team;
using ProjectStructure.Application.Models.Team;

namespace ProjectStructure.Application.MappingProfiles
{
    public sealed class TeamProfile: Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamViewModelDTO>();
            CreateMap<TeamCreatedDTO, TeamUpdateDTO>();
        }
    }
}
