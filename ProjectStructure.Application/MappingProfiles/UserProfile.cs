﻿using AutoMapper;

using EF.Core.DAL.Entities;
using ProjectStructure.Application.Common.Models.User;
using ProjectStructure.Application.Models.User;

namespace  ProjectStructure.Application.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<UserCreatedDTO, User>();
        }
    }
}
