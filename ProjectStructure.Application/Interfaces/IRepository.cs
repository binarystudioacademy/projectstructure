﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Application.Interfaces
{
    public interface IRepository<T>
          where T : class
    {
        Task<IList<T>> GetAll();
        Task<T> GetById(int id);
        Task Create(T item);
        Task Update(T item);
        Task Delete(int id);
    }
}
